## Tasks

This details Ron's system design.

1. Install NXLog agent on IIS server

    a. Configure [IIS Server][iis] to output logs by site or server

2. Install Firebeat agent on Linux servers

    a. [Apache (Univision)][apache]

    b. [Nginx][nginx]

3. Install Firebeat agents on other servers such as:

    a. PHP logs

    b. C# logs

4. AWS Dynamodb

    a. Install and working dynamodb instance

    b. Submit suggested schema for log collection for review

    c. Create Schema in repository

5. Workflow

    a. Data being collected from agents 

    b. Data pushed through Logstash

    c. Data inserted into Dynamodb

6. SNS Alerting Mechanism

    a. Lambda trigger on data insertion that runs analyzation on new data

        i. Error alerting for PHP or .Net errors
        ii. Error alerting for JavaScript errors
        iii. HTTP Error alerting for 400 - 511

7. Domo Visualization

    a. Connect to DynamoDB datasets

    b. Bring in datasets into Domo for reporting

    c. Create Historical bar chart visualizations with trend lines.

[iis]: https://docs.microsoft.com/en-us/iis/manage/provisioning-and-managing-iis/configure-logging-in-iis#prerequisites
[apache]: https://httpd.apache.org/docs/1.3/logs.html
[nginx]: https://docs.nginx.com/nginx/admin-guide/monitoring/logging/