# RDI Server Log Pipeline Project

Author: Tommy Truong, MarTech Engineering Intern '17

## Introduction

This project details the server log pipeline that will collect, visualize and 
notify recipients of server health and sanity checks.
v
This system collect logs from the different servers that Red Door maintains and
collects them via lightweight agents such as NXLog and Elastic's Firebeat.

The agents will push events/logs to a Logstash client in a Docker container that
is hosted in Amazon's Elastic Container Service. The Logstash image will be held
in Amazon ECR.

The Logstash client will output the events to an AWS S3 bucket. Upon insertion,
the bucket will send nodifications via Amazon's Simple Notification Service,
which will notify RDI recipients via SMS or email alerts through AWS Lambda.

The logs will be visualized with Domo using an AWS DynamoDB Domo Connector.

## Notes

docker run --rm --network=host -p 514:3516 logstash

*AWS ECR/ECS*

The ARN and URifor the AWS ECR repository is arn:aws:ecr:us-west-2:610886666897:repository/elk_stack elk_stack and 610886666897.dkr.ecr.us-west-2.amazonaws.com/elk_stack

## References

1. [Logstash Docs][logstash]

    a. [Logstash SNS Output Plugin][logstash sns]

    b. [Logstash Docker Container on Amazon ECS][logstash AWS ECS]

    c. [Logstash, NXLog, Elasticsearch][logstash NXLog elastic]

2. [AWS SNS Docs][AWS SNS]
    
    a. [AWS SNS and Lambda][AWS SNS Lambda]

3. [Logstash different dir, same filenames bug][logstash filenames] 

    a. [NXLog - Multiple IIS site config][loggly]

4. [Docker - local testing][docker local test]

5. [NXLog - IIS][NXLog IIS]

    a. [NXLog - om_tcp][NXLog tcp]

6. [Deploy Logstash Docker Image on AWS ECS/ECR][Deploy AWS]

[logstash]: https://www.elastic.co/guide/en/logstash/6.2/index.html
[logstash sns]: https://www.elastic.co/guide/en/logstash/6.2/plugins-outputs-sns.html
[logstash AWS ECS]: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html
[logstash NXLog elastic]: http://edbaker.weebly.com/blog/iis-logstash-elasticsearch-nxlog
[AWS SNS]: https://docs.aws.amazon.com/sns/latest/dg/welcome.html
[AWS SNS Lambda]: https://docs.aws.amazon.com/sns/latest/dg/sns-lambda.html
[logstash filenames]: https://blog.sstorie.com/importing-iis-logs-into-elasticsearch-with-logstash/
[loggly]: https://www.loggly.com/docs/iis-web-server-logs/
[docker local test]: http://www.frommknecht.net/local-logstash-testing/
[NXLog IIS]: https://medium.com/@devfire/deploying-the-elk-stack-on-amazon-ecs-dd97d671df06
[NXLog tcp]: https://nxlog.co/documentation/nxlog-user-guide#om_tcp
[Deploy AWS]: https://medium.com/@devfire/deploying-the-elk-stack-on-amazon-ecs-dd97d671df06